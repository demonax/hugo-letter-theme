/* Open when someone clicks on the span element */
function openNav() {
  document.getElementById("navMenuOverlay").style.width = "100%";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
  document.getElementById("navMenuOverlay").style.width = "0%"; 
}

function openSub() {  
  document.getElementById("navMenuOverlay").classList.add("visible");
}

function closeSub() {
  document.getElementById("navMenuOverlay").classList.remove("visible");
}
